function mensajeElimina(){
    Swal.fire({
        title: '¿Seguro que desea eliminar el contenido?',
        showDenyButton: true,
        denyButtonText: `NO`,
        confirmButtonText: `SI`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          Swal.fire('Eliminado', '', 'success')
        }else if (result.isDenied) {
            Swal.fire('No se borro ningún elemento', '', 'error')
          }
      })
}