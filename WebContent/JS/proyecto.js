
const contenedor = document.getElementById("proyecto-mostrador");
let proyectos;

consultaProyectos();
 
class Proyecto{
	constructor(id,nombre,descripcion,fechaEntrega){
		this.id = id,
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaEntrega = fechaEntrega;
	}
}

function consultaProyectos(){
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", "../ConsultaProyectos", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(null);
	xhttp.onreadystatechange = function(){
		if(this.readyState === 4 && this.status === 200){
			proyectos = JSON.parse(this.responseText);
			if(proyectos !== null){
				console.log(proyectos);
				for(let p in proyectos){
					dibujaProyecto(proyectos[p]);
				}
			}
		}
	};
}

function altaProyectos(){
	let xhttp = new XMLHttpRequest();
	xhttp.open("POST", "../AltaProyectos", false);
	xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	let aux = new Set(proyectos);
	xhttp.send("proyectos=" + JSON.stringify(Array.from(aux)));
	xhttp.onreadystatechange = function(){
		if(this.readyState === 4 && this.status === 200){
			res = JSON.parse(this.responseText);
			console.log(res);
        }
    } 
}

function bajaProyecto(idBorrar){
	let xhttp = new XMLHttpRequest();
	xhttp.open("POST", "../BajaProyecto", true);
	xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhttp.send("id=" + idBorrar);
	xhttp.onreadystatechange = function(){
		if(this.readyState === 4 && this.status === 200){
			res = JSON.parse(this.responseText);
			console.log(res);
        }
    } 
}

function agregarProyecto(){
	let nombreProyecto = document.getElementById("nombreProyecto");
	let fechaProyecto = document.getElementById("fechaProyecto");
	let descripcionProyecto = document.getElementById("descripcionProyecto");
	let proyecto;
	if(proyectos !== null){
		proyecto = new Proyecto(proyectos[proyectos.length-1].id+1, nombreProyecto.value, descripcionProyecto.value, fechaProyecto.value + " 00:00");
		proyectos[proyectos.length] = {
			id : proyectos.id,
			nombre: proyecto.nombre,
			descripcion : proyecto.descripcion,
			fechaEntrega : proyecto.fechaEntrega
		}
		console.log(proyectos);
		dibujaProyecto(proyecto);
		altaProyectos();
	}else{
		proyecto = new Proyecto(0, nombreProyecto.value, descripcionProyecto.value, fechaProyecto.value + " 00:00");
		proyectos = [];
		proyectos[0] = {
			id : proyecto.id,
			nombre: proyecto.nombre,
			descripcion : proyecto.descripcion,
			fechaEntrega : proyecto.fechaEntrega
		}
		console.log(proyectos);
		altaProyectos();
		consultaProyectos();
	}
}

function dibujaProyecto(proyecto){
	
	var divProyecto = document.createElement("div");
	divProyecto.setAttribute("class", 'accordion item');
	divProyecto.setAttribute("id", 'elemento ' + proyecto.id);
	
	var h2Proyecto = document.createElement("h2");
	h2Proyecto.setAttribute("class", 'accordion-header');
	h2Proyecto.setAttribute("id", 'proyecto-' + proyecto.id);
	
	var buttonClass = document.createElement("button");
	buttonClass.setAttribute("class", 'accordion-button collapsed');
	buttonClass.setAttribute("type", 'button');
	buttonClass.setAttribute("data-bs-toggle", 'collapse');
	buttonClass.setAttribute("data-bs-target", '#collapse-' + proyecto.id);
	buttonClass.setAttribute("aria-expanded", 'false');	
	buttonClass.setAttribute("aria-controls", 'collapse-' + proyecto.id);
	buttonClass.textContent = proyecto.nombre.toUpperCase();
	
	
	var infoProyecto = document.createElement("div");
	infoProyecto.setAttribute("id", 'collapse-' + proyecto.id);
	infoProyecto.setAttribute("class", 'accordion-collapse collapse');	
	infoProyecto.setAttribute("aria-labelledby", 'proyecto-' + proyecto.id);
	infoProyecto.setAttribute("data-bs-parent", '#proyecto-mostrador');
	
	let cuerpoProyecto = document.createElement("div");
	cuerpoProyecto.setAttribute("class", 'accordion-body');
	

	/* Para dar el formato a la fecha */
	let fechaFormato = proyecto.fechaEntrega.split("-");
	let mesesArray = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];


	let fechaImprime = "Entrega el " + parseInt(fechaFormato[2]) + " de " + mesesArray[parseInt(fechaFormato[1]) - 1] +": ";
	let fechaNegrita = document.createElement("strong");
	fechaNegrita.textContent = fechaImprime;
	
	let span = document.createElement("span");
	span.textContent = proyecto.descripcion;
	
	
	let iconoElimina = document.createElement("i");
	iconoElimina.setAttribute("class", 'fas fa-times icono icono-elimina');
	iconoElimina.setAttribute("onclick", 'borraProyecto(' + proyecto.id + ')');
	

	let vinculoTareas = document.createElement("a");
	vinculoTareas.setAttribute("href", 'tareas.jsp?id=' + proyecto.id);



	let iconoEntra = document.createElement("i");
	iconoEntra.setAttribute("class", 'fas fa-angle-double-right icono icono-entra');
	
	contenedor.appendChild(divProyecto);
	divProyecto.appendChild(h2Proyecto);
	divProyecto.appendChild(infoProyecto);
	h2Proyecto.appendChild(buttonClass);
	infoProyecto.appendChild(cuerpoProyecto);
	cuerpoProyecto.appendChild(fechaNegrita);
	cuerpoProyecto.appendChild(span);
	cuerpoProyecto.appendChild(iconoElimina);
	cuerpoProyecto.appendChild(vinculoTareas);
	vinculoTareas.appendChild(iconoEntra);
	
}

function borraProyecto(idBorrar){
	Swal.fire({
        title: '¿Seguro que desea eliminar el proyecto?',
        showDenyButton: true,
        denyButtonText: `NO`,
        confirmButtonText: `SI`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
			let hijoBorrado = document.getElementById("elemento " + idBorrar);
			contenedor.removeChild(hijoBorrado);
			for(let i=0; i<proyectos.length; i++){
				if(proyectos[i].id === idBorrar){
					if(proyectos.length == 1){
						proyectos = null;
					}else{
						proyectos.splice(i, 1);
					}
					break;
				}
			}
			bajaProyecto(idBorrar);
		  	Swal.fire('Eliminado', '', 'success')
        }else if (result.isDenied) {
            Swal.fire('No se borro ningún elemento', '', 'error')
          }
      })	
}