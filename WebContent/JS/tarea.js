
let idProyectoGlobal = parseInt(document.getElementById("idProyecto").value);

let tareas;

let idSeleccionado = null;

consultaTareas();


function consultaTareas(){
	let xhttp = new XMLHttpRequest();
	xhttp.open("POST", "../ConsultaTareas", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhttp.send("id=" + idProyectoGlobal);
	xhttp.onreadystatechange = function(){
		if(this.readyState === 4 && this.status === 200){
            tareas = JSON.parse(this.responseText);
            console.log(tareas);
            if(tareas !== null){
                console.log(tareas);
                pintaTareas();
            }
		}
    };
}

function altaTareas(){
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../AltaTareas", false);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    let aux = new Set(tareas);
    xhttp.send("tareas=" + JSON.stringify(Array.from(aux)));
    xhttp.onreadystatechange = function(){
		if(this.readyState === 4 && this.status === 200){
			res = JSON.parse(this.responseText);
			console.log(res);
        }
    }   
}

function bajaTareas(idBorrar){
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../BajaTarea", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send("id=" + idBorrar);
    xhttp.onreadystatechange = function(){
		if(this.readyState === 4 && this.status === 200){
			res = JSON.parse(this.responseText);
			console.log(res);
        }
    }   
}

function pintaTareas(){
    tareas.forEach((item, index, array) => pintaTarea(item));
}

function pintaTarea(tarea){
    
    let divNom = document.getElementById("contiene-tareas-id");

    let aNom = document.createElement("a");
    aNom.setAttribute("class", 'btn btn-outline-primary nombre-tareas');
    aNom.setAttribute("href", '#tarea-' + tarea.id);
    aNom.setAttribute("id", 'titulo-tarea-' + tarea.id);
    aNom.textContent = tarea.nombre.toUpperCase();

    let colDes = document.getElementById("colDescripcion");

    let divRowTarea = document.createElement("div");
    divRowTarea.setAttribute("class","row tarea-esquema");
    divRowTarea.setAttribute("id", "tarea-" + tarea.id);


    /* Para dar el formato a la fecha */
	let fechaFormato = tarea.fechaInicio.split("-");
    let mesesArray = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    let fechaImprime = "Empezado el " + parseInt(fechaFormato[2]) + " de " + mesesArray[parseInt(fechaFormato[1]) - 1] +": ";

    let lblDes = document.createElement("label");
    lblDes.setAttribute("class", 'descripcion-tarea');
    lblDes.textContent  = fechaImprime + " " + tarea.descripcion;
    

    let divRow = document.createElement("div");
    divRow.setAttribute("class", "row");
    
    let divCol = document.createElement("div");
    divCol.setAttribute("class", "col");

    let divProgress = document.createElement("div");
    divProgress.setAttribute("class", "progress");

    let divBar = document.createElement("div");
    divBar.setAttribute("class", "progress-bar progress-bar-striped progress-bar-animated bg-warning");
    divBar.setAttribute("id", 'barra-progreso-' + tarea.id)
    divBar.setAttribute("role", "progressbar");
    divBar.setAttribute("aria-valuenow", tarea.avance);
    divBar.style.width = tarea.avance + "%";
    divBar.setAttribute("aria-valuemin", "0");
    divBar.setAttribute("aria-valuemax", "100");

    let divBotones = document.createElement("div");
    divBotones.setAttribute("class", "contiene-botones");

    let aModificar = document.createElement("a");
    aModificar.setAttribute("href","#modifica-tarea");
    aModificar.setAttribute("class","btn btn-warning separa-botones");
    aModificar.setAttribute("onclick", 'seleccionaModificar(' + tarea.id + ')');
    aModificar.textContent = "MODIFICAR";

    let btnEliminar = document.createElement("button");
    btnEliminar.setAttribute("type","button");
    btnEliminar.setAttribute("class","btn btn-danger");
    btnEliminar.setAttribute("onclick", 'borrarTarea(' + tarea.id + ')');
    btnEliminar.textContent = "ELIMINAR";

    divProgress.appendChild(divBar);
    divCol.appendChild(divProgress);
    divRow.appendChild(divCol);

    divBotones.appendChild(aModificar);
    divBotones.appendChild(btnEliminar);

    divRowTarea.appendChild(lblDes);
    divRowTarea.appendChild(divRow);
    divRowTarea.appendChild(divBotones);

    colDes.appendChild(divRowTarea);

    divNom.appendChild(aNom);
}

function agregaTarea(){

    let txtNombre = document.getElementById("txtNombre");
    let txtProgreso = document.getElementById("txtProgreso");
    let txtDescripcion = document.getElementById("txtDescripcion");
    let fechaTarea = document.getElementById("fechaTarea");
    let t;

    if(tareas !== null){
        t = new Tarea(tareas[tareas.length-1].id+1, idProyectoGlobal, txtNombre.value, txtDescripcion.value, 0, 0, txtProgreso.value, fechaTarea.value + " 00:00");
        tareas[tareas.length] = {
            id: t.id,
            idProyecto: t.idProyecto,
            nombre: t.nombre,
            descripcion: t.descripcion,
            duracion: t.duracion,
            predecesor: t.predecesor,
            avance: t.avance,
            fechaInicio: t.fechaInicio
        };
        console.log(tareas);
	    pintaTarea(t);
	    altaTareas();
    }else{
        t = new Tarea(0, idProyectoGlobal, txtNombre.value, txtDescripcion.value, 0, 0, txtProgreso.value, fechaTarea.value + " 00:00");
        tareas = [];
        tareas[0] = {
            id: t.id,
            idProyecto: t.idProyecto,
            nombre: t.nombre,
            descripcion: t.descripcion,
            duracion: t.duracion,
            predecesor: t.predecesor,
            avance: t.avance,
            fechaInicio: t.fechaInicio
        };
        console.log(tareas);
        altaTareas();
        consultaTareas();
    }
}

function seleccionaModificar(id){
    idSeleccionado = id;
}

function modificarTarea(){
    avance = document.getElementById("progresoTarea");
    barraModificada = document.getElementById("barra-progreso-" + idSeleccionado);
    barraModificada.style.width = avance.value + "%";
    barraModificada.setAttribute("aria-valuenow", avance.value);
    for(let i=0; i<tareas.length; i++){
        if(idSeleccionado === tareas[i].id){
            tareas[i].avance = avance.value;
            break;
        }
    }
    altaTareas();
    console.log(tareas);
}


function  borrarTarea(idBorrar){

    Swal.fire({
        title: '¿Seguro que desea eliminar la tarea?',
        showDenyButton: true,
        denyButtonText: `NO`,
        confirmButtonText: `SI`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {


            let contenidoTarea = document.getElementById("tarea-" + idBorrar);
            let tituloTarea = document.getElementById("titulo-tarea-" + idBorrar);
            let padre1 = document.getElementById("contiene-tareas-id");
            let padre2 = document.getElementById("colDescripcion");
            padre1.removeChild(tituloTarea);
            padre2.removeChild(contenidoTarea);
            for(let i=0; i<tareas.length; i++){
                if(tareas[i].id === idBorrar){
                    if(tareas.length === 1){
                        tareas = null;
                    }else{
                        tareas.splice(i, 1);
                    }
                    break;
                }
            }
            bajaTareas(idBorrar);
            Swal.fire('Eliminado', '', 'success')
              

        }else if (result.isDenied) {
            Swal.fire('No se borro ningún elemento', '', 'error')
          }
      })

      
}

class Tarea{
    constructor(id, idProyecto, nombre, descripcion, duracion, predecesor, avance, fechaInicio){
        this.id = id;
        this.idProyecto = idProyecto;  
        this.nombre = nombre;          
        this.descripcion = descripcion;
        this.duracion = duracion;      
        this.predecesor = predecesor;  
        this.avance = avance;          
        this.fechaInicio = fechaInicio;
    }
}