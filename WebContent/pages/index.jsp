<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Icono de la pagina -->
    <link rel="icon" type="image/png" href="../images/tarea-img.png"/>
    <!-- CSS -->
    <link href="../css/estilo-index.css" rel="stylesheet">
    <!-- Bootstrap CSS & JS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <!-- FUENTES -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=East+Sea+Dokdo&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <!-- ICONS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="50375849-578C-4D69-8034-AC5A9AC921AC" crossorigin="anonymous">
    <!--ALERTAS-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>To Do</title>
</head>

<body>
    <header class="cabecera-pagina">
        <h2 class="nombre-app">TO DO</h2>
        <h2 class="lema-app"> ORGANIZA TUS TAREAS Y PROYECTOS</h2>    
    </header>

    <h1 class="titulo-principal">TUS PROYECTOS</h1>
    <div class="container muestra-proyectos">
        <div id="proyecto-mostrador"> 
          <!-- Aqui se insertaran los datos desde el js -->  
        </div>
        <div class="d-grid gap-2 agrega-boton">
          <a href="#agrega-proyecto" class="btn btn-primary">AGREGAR PROYECTO</a>
        </div>
    </div>

    <div class="ventana-emergente" id="agrega-proyecto">
      <div class="ventana-saliente">
        <div class="img"></div>
        <div class="contiene-form">
          <h1>AGREGA LOS DATOS DEL PROYECTO:</h1>
          <form method='post' action="index.jsp">
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="nombreProyecto" name="nombreProyecto" placeholder="To Do" required>
              <label for="nombreProyecto">Nombre del Proyecto</label>
            </div>
            <div class="form-floating">
              <input type="date" class="form-control" id="fechaProyecto" name="fechaProyecto" required>
              <label for="fechaProyecto">Fecha:</label>
            </div>
            <br>
            <div class="form-floating">
              <textarea class="form-control" placeholder="Coloca la descripción del proyecto" id="descripcionProyecto" name="descripcionProyecto" required></textarea>
              <label for="descripcionProyecto">Descripci&oacute;n</label>
            </div>
            <br>
            <div class="d-grid gap-2 agrega-boton">
              <a href="#" type="submmit" class="btn btn-success" name="AgregaProyecto" onclick="agregarProyecto()">AGREGAR</a>
            </div>
          </form>
        </div>
        <div><a href="#" class="btn-cerrar-ventana">X</a></div>
      </div>
    </div> 
	</body>
	 <!-- JS -->
    <script src="../JS/proyecto.js"></script> 
</html>

