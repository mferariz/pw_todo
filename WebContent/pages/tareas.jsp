<%@page import="com.upiicsa.modelo.Proyecto"%>
<%@page import="com.upiicsa.dao.ProyectoDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>	
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Icono de la pagina -->
        <link rel="icon" type="image/png" href="../images/tarea-img.png"/>
        <!-- CSS -->
        <link href="../css/estilo-infoProyecto.css" rel="stylesheet">
        <!-- Bootstrap CSS & JS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        <!-- FUENTES -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=East+Sea+Dokdo&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
        <!-- ICONS -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="50375849-578C-4D69-8034-AC5A9AC921AC" crossorigin="anonymous">
        <!--ALERTAS-->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <!--JS-->
        <script src="../JS/alertas.js"></script>
        <title>To Do</title>
    </head>
    
    <%
    	int idProyecto = Integer.parseInt(request.getParameter("id"));
    	ProyectoDao proyectoMuestra = new ProyectoDao();
    	Proyecto proyecto = proyectoMuestra.Consulta(idProyecto);
    	String nombreProyecto = proyecto.getNombre().toUpperCase();
    %>
    
    <body>
        <header class="cabecera-pagina">
            <a href="./index.jsp"><h2 class="nombre-app">TO DO</h2></a>
            <h2 class="lema-app"> <%=nombreProyecto%></h1>    
        </header>
        
        <input type="hidden" value='<%=idProyecto%>' name="idProyecto" id="idProyecto">
        
        <div class="container contiene-tareas" id="todo-tareas">
            <div class="row">
                <div id="colNombre" class="col-md mueve-tareas">
                    <h1 class="titulo-tareas">TUS TAREAS</h1>
                    <div class="d-grid gap-2" id="contiene-tareas-id">

                    </div>
                </div>
                <div class="col-md-8">
                    <h1 class="titulo-tareas">DESCRIPCION Y AVANCE</h1>
                    <div id="colDescripcion" class="scroll-tareas">
                         
                    </div>                 
                </div>
            </div>
        </div>
        <div class="container contiene-acciones">
                <a href="#agrega-tarea" class="btn btn-success boton-accion-1">AGREGAR TAREA</button></a>
        </div>
        
        <div class="ventana-emergente" id="agrega-tarea">
            <div class="ventana-saliente">
                <div class="contiene-form">
                    <h1>AGREGA LOS DATOS DE LA TAREA:</h1>
                    <form>
                        <div class="form-floating mb-3">
                            <input id="txtNombre" type="text" class="form-control" id="nombreTarea" placeholder="Agregar datos">
                            <label for="nombreTarea">Nombre de la Tarea</label>
                        </div>
                        <div class="form-floating">
                            <input id="txtProgreso" type="number" class="form-control" id="progresoTarea" placeholder="50%">
                            <label for="progresoTarea">Progreso:</label>
                        </div>
                        <br>
                        <div class="form-floating">
                            <input type="date" class="form-control" id="fechaTarea" name="fechaTarea" required>
                            <label for="fechaTarea">Fecha:</label>
                          </div>
                          <br>
                        <div class="form-floating">
                            <textarea id="txtDescripcion" class="form-control" placeholder="Coloca la descripción de la tarea" id="descripcionTarea"></textarea>
                            <label for="descripcionTarea">Descripci&oacute;n</label>
                        </div>
                        <br>
                        <div class="d-grid gap-2 agrega-boton">
                            <a href="#" type="button" class="btn btn-primary" onclick="agregaTarea()">AGREGAR</a>
                        </div>
                    </form>
                </div>
                <div class="img"></div>
                <div><a href="#" class="btn-cerrar-ventana">X</a></div>
            </div>
          </div>

          <div class="ventana-emergente-modifica" id="modifica-tarea">
            <div class="ventana-saliente-modifica">
                <div class="img-modifica"></div>
                <div class="contiene-form">
                    <h1>AGREGA EL CAMBIO A LA TAREA:</h1>
                    <form>
                        <div class="form-floating">
                            <input type="number" class="form-control" id="progresoTarea" placeholder="50%">
                            <label for="progresoTarea">Progreso:</label>
                        </div>
                        <br>
                        <div class="d-grid gap-2 agrega-boton">
                            <a href="#" type="button" class="btn btn-danger" onclick="modificarTarea()">GUARDAR CAMBIO</a>
                        </div>
                    </form>
                </div>
                <div><a href="#" class="btn-cerrar-ventana">X</a></div>
            </div>
          </div>
    </body>
    <!-- JS -->
    <script src="../JS/tarea.js"></script>
</html>