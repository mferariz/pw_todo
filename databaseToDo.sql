drop schema if exists ToDo;
create schema ToDo;
use ToDo;

create table proyecto(
	id numeric(10) not null primary key,
    nombre varchar(150) not null,
    descripcion text(500),
    fechaEntrega datetime default now()
);

create table tarea(
	id numeric(10) not null primary key,
    idProyecto numeric(10) not null, 
    nombre varchar(200) not null,
    descripcion text(500),
    duracion numeric(4) default 5,
    predecesor numeric(4) default 0,
    avance numeric(3),
    fechaInicio datetime default now(),
    foreign key (idProyecto) references Proyecto(id) 
);

delimiter :v

drop procedure if exists sp_AltaProyecto :v

create procedure sp_AltaProyecto(in i numeric(10), in nom varchar(150), in des text(500), in fEnt datetime, out msj varchar(120)) begin 

declare existe numeric(1);
declare newId numeric(10);
set existe = (select count(id) from proyecto where id = i);

if(existe) then 
	update proyecto set nombre = nom, descripcion = ifnull(des, "Sin descripcion") where id = i;
    set msj = "Proyecto actualizado correctamente";
else
	set newId = (select ifnull(max(id), 0) from proyecto) + 1;
	insert into proyecto values(newId, nom, ifnull(des, "Sin descripcion"), fEnt);
    set msj = "Proyecto creado correctamente";
 end if;

end :v

drop procedure if exists sp_AltaTarea :v
create procedure sp_AltaTarea(in i numeric(10), in idP numeric(10), in nom varchar(150), in des text(500),
in dur numeric(4), in pre numeric(4), in ava numeric(3), in fIni datetime, out msj varchar(120)) begin 

declare existe numeric(1);
declare newId numeric(10);
set existe = (select count(id) from tarea where id = i);

if(existe) then 
    update tarea set nombre = nom, descripcion = ifnull(des, "Sin descripcion"), duracion = dur, predecesor = pre, avance = ava, fechaInicio = fIni where id = i;
    set msj = "Tarea actualizada exitosamente";
else
	set newId = (select ifnull(max(id), 0) from tarea) + 1;
	insert into tarea values(newId, idP, nom, ifnull(des, "Sin descripcion"), dur, pre, ava, fIni);
    set msj = "Tarea creada exitosamente";
 end if;

end :v

drop procedure if exists sp_BajaProyecto :v
create procedure sp_BajaProyecto(in i numeric(10), out msj varchar(150)) begin

declare cont numeric(3);
set cont = (select count(id) from tarea where idProyecto = i);

if(cont > 0) then
	delete from tarea where idProyecto = i;
    set msj = concat("Se han eliminado ", cont, " tareas junto con el proyecto.");
else
    set msj = "se han eliminado 0 tareas";
end if;

delete from proyecto where id = i;

end :v

delimiter ;

call sp_AltaProyecto(0,"Proyecto PW", null, "2021-12-8", @aux);
call sp_AltaProyecto(0,"To Do", "Organizador de Tareas", "2021-12-22", @aux);
call sp_AltaProyecto(0,"QRreader App", "Lectura y despliegue de QRs", "2021-12-21", @aux);
call sp_AltaProyecto(0,"BanApp", "Bloquea Aplicaciones del sistema", "2021-12-10", @aux);
call sp_AltaProyecto(0,"Proyecto de prueba", "El comienzo del final...", "2020-04-02", @aux);
-- select(@aux); --
call sp_AltaTarea(0, 1, "Tarea PW1", "Descripcion 1", 1, 0, 25, "2021-12-12", @aux2);
call sp_AltaTarea(0, 1, "Tarea PW2", "Descripcion 2", 1, 0, 25, "2021-12-12", @aux2);
call sp_AltaTarea(0, 1, "Tarea PW3", null, 1, 0, 25, "2021-12-12", @aux2);

select * from proyecto;
select * from tarea;


