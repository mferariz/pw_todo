package com.upiicsa.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.upiicsa.dao.ProyectoDao;
import com.upiicsa.modelo.Proyecto;
import com.upiicsa.utils.LocalDateTimeAdapter;

@WebServlet("/AltaProyectos")
public class AltaProyectos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AltaProyectos() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String data = request.getParameter("proyectos");
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter()).create();
		String mensaje = "No hay parametros";
		
		if(data != null) {
			List<Proyecto> proyectos = gson.fromJson(data, new TypeToken<List<Proyecto>>() {}.getType());
			
			ProyectoDao pDao = new ProyectoDao();
			for(Proyecto p: proyectos) {
				mensaje = pDao.Cambio(p);
			}
		} 
		response.setContentType("application/json");
		response.getWriter().print(gson.toJson(mensaje));
		
	}

}
