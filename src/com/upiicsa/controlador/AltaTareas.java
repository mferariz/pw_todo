package com.upiicsa.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.upiicsa.dao.TareaDao;
import com.upiicsa.modelo.Tarea;
import com.upiicsa.utils.LocalDateTimeAdapter;

@WebServlet("/AltaTareas")
public class AltaTareas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AltaTareas() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String data = request.getParameter("tareas");
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter()).create();
		String mensaje = "Esta vacio";
		
		if(data != null) {
			List<Tarea> tareas = gson.fromJson(data, new TypeToken<List<Tarea>>() {}.getType());
			
			TareaDao tDao = new TareaDao();
			for(Tarea t: tareas) {
				mensaje = tDao.Cambio(t);
			}
		}
		response.setContentType("application/json");
		response.getWriter().print(gson.toJson(mensaje));
	}

}
