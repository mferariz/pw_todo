package com.upiicsa.controlador;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.upiicsa.dao.TareaDao;
import com.upiicsa.utils.LocalDateTimeAdapter;

@WebServlet("/BajaTarea")
public class BajaTarea extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BajaTarea() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mensaje;
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter()).create();
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			TareaDao tDao = new TareaDao();
			mensaje = tDao.Baja(id);
		} catch (Exception e) {
			mensaje = "Parametro incorrecto.";
		}
		response.setContentType("application/json");
		response.getWriter().print(gson.toJson(mensaje));
	}

}
