package com.upiicsa.controlador;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.upiicsa.dao.TareaDao;
import com.upiicsa.modelo.Tarea;
import com.upiicsa.utils.LocalDateTimeAdapter;

@WebServlet("/ConsultaTareas")
public class ConsultaTareas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ConsultaTareas() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id = Integer.parseInt(request.getParameter("id"));
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter()).create();
		
		TareaDao tDao = new TareaDao();
		List<Tarea> tareas = tDao.ConsultaIdProyecto(id);
		
		response.setContentType("application/json");
		response.getWriter().print(gson.toJson(tareas));
		//doGet(request, response);
	}

}
