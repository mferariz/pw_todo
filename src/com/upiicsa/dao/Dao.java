package com.upiicsa.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface Dao<Object>{
	
	public String Alta(Object o);
	public String Baja(Object o);
	public String Baja(int id);
	public String Cambio(Object o);
	public Object Consulta(int id);
	public List<Object> TraeTodos();
	
	public Object Traducir(ResultSet rs) throws SQLException;
	
}
