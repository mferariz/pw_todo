package com.upiicsa.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.upiicsa.modelo.Proyecto;
import com.upiicsa.utils.Conexion;

public class ProyectoDao implements Dao<Proyecto>{
	
	private final Conexion cx = new Conexion();
	//private final String ALTA = "{call sp_AltaProyecto(0,?,?,?)}"; 
	private final String BAJA = "{call sp_BajaProyecto(?,?)}";
	private final String CAMBIO = "{call sp_AltaProyecto(?,?,?,?,?)}";
	private final String CONSULTA = "select * from proyecto where id = ?";
	private final String TODOS = "select * from proyecto";

	@Override
	public String Alta(Proyecto o) {
		o.setId(0);
		return Cambio(o);
	}

	@Override
	public String Baja(Proyecto o) {
		return Baja(o.getId());
	}

	@Override
	public String Baja(int id) {
		String msj;
		try{
			Connection cn = cx.abreConexion();
			CallableStatement st = cn.prepareCall(BAJA);
			st.setInt(1, id);
			
			st.registerOutParameter(2, Types.VARCHAR);
			
			st.execute();
			msj = st.getString(2);
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
			msj = "Codigo de error sql: " + sqlEx.getErrorCode();
		} catch (Exception e) {
			msj = "Error desconocido: " + e.getMessage();
		}
		return msj;
	}

	@Override
	public String Cambio(Proyecto o) {
		String msj;
		try{
			Connection cn = cx.abreConexion();
			CallableStatement st = cn.prepareCall(CAMBIO);
			st.setInt(1, o.getId());
			st.setString(2, o.getNombre());
			st.setString(3, o.getDescripcion());
			st.setTimestamp(4, java.sql.Timestamp.valueOf(o.getFechaEntrega()));
			
			st.registerOutParameter(5, Types.VARCHAR);
			
			st.execute();
			msj = st.getString(5);
			//msj = "Proyecto guardado con exito";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
			if(sqlEx.getErrorCode() == Conexion.MYSQL_DUPLICATE_PK){
                msj = "Clave del proyecto ya existente.";
            } else {
            	sqlEx.printStackTrace();
                msj = "Codigo de error sql: " + sqlEx.getErrorCode();
            }

		} catch (Exception e) {
			msj = "Error desconocido: " + e.getMessage();
		}
		
		return msj;
	}

	@Override
	public Proyecto Consulta(int id) {
		Proyecto proyecto; 
		List<Proyecto> proyectos = new ArrayList<Proyecto>();
		try{
			Connection cn = cx.abreConexion();
			PreparedStatement st = cn.prepareStatement(CONSULTA);
			st.setInt(1, id);
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()) {
				proyectos.add(Traducir(rs));
			}
			if(proyectos.size() == 1) {
				proyecto = proyectos.get(0);
			} else {
				proyecto = null;
			}
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
        	sqlEx.printStackTrace();
            System.out.println("Codigo de error sql: " + sqlEx.getErrorCode());
            proyecto = null;
		} catch (Exception e) {
			System.out.println("Error desconocido: " + e.getMessage());
			proyecto = null;
		}
		
		return proyecto;
	}

	@Override
	public List<Proyecto> TraeTodos() {
		List<Proyecto> proyectos = new ArrayList<Proyecto>();
		try{
			Connection cn = cx.abreConexion();
			PreparedStatement st = cn.prepareStatement(TODOS);
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()) {
				proyectos.add(Traducir(rs));
			}
			if(proyectos.isEmpty()) {
				proyectos = null;
			}
			//msj = "Tarea guardada con exito";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
        	sqlEx.printStackTrace();
            System.out.println("Codigo de error sql: " + sqlEx.getErrorCode());
            proyectos = null;
		} catch (Exception e) {
			System.out.println("Error desconocido: " + e.getMessage());
			proyectos = null;
		}
		
		return proyectos;
	}

	@Override
	public Proyecto Traducir(ResultSet rs) throws SQLException {
		return new Proyecto(rs.getInt("id"), rs.getString("nombre"), rs.getString("descripcion"), rs.getTimestamp("fechaEntrega").toLocalDateTime());
	}

}
