package com.upiicsa.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.upiicsa.modelo.Tarea;
import com.upiicsa.utils.Conexion;

public class TareaDao implements Dao<Tarea>{
	
	private final Conexion cx = new Conexion();
	//private final String ALTA = "{call sp_AltaTarea(0,?,?,?,?,?,?,?)}"; 
	private final String BAJA = "delete from tarea where id = ?";
	private final String CAMBIO = "{call sp_AltaTarea(?,?,?,?,?,?,?,?,?)}";
	private final String CONSULTA = "select * from tarea where id = ?";
	private final String CONSULTA_ID_PROYECTO = "select * from tarea where idProyecto = ?";
	private final String TODOS = "select * from tarea";
	
	@Override
	public String Alta(Tarea o) {
		o.setId(0);
		return Cambio(o);
	}

	@Override
	public String Baja(Tarea o) {
		return Baja(o.getId());
	}

	@Override
	public String Baja(int id) {
		String msj;
		try{
			Connection cn = cx.abreConexion();
			PreparedStatement st = cn.prepareStatement(BAJA);
			st.setInt(1, id);
			
			st.executeUpdate();
			msj = "Tarea eliminada con exito.";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
			msj = "Codigo de error sql: " + sqlEx.getErrorCode();
		} catch (Exception e) {
			msj = "Error desconocido: " + e.getMessage();
		}
		
		return msj;
	}

	@Override
	public String Cambio(Tarea o) {
		String msj;
		try{
			Connection cn = cx.abreConexion();
			CallableStatement st = cn.prepareCall(CAMBIO);
			st.setInt(1, o.getId());
			st.setInt(2, o.getIdProyecto());
			st.setString(3, o.getNombre());
			st.setString(4, o.getDescripcion());
			st.setInt(5, o.getDuracion());
			st.setInt(6, o.getPredecesor());
			st.setInt(7, o.getAvance());
			st.setTimestamp(8, java.sql.Timestamp.valueOf(o.getFechaInicio()));
			
			st.registerOutParameter(9, Types.VARCHAR);
			
			st.execute();
			msj = st.getString(9);
			//msj = "Tarea guardada con exito";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
			if(sqlEx.getErrorCode() == Conexion.MYSQL_DUPLICATE_PK){
                msj = "Clave de tarea ya existente.";
            } else {
            	sqlEx.printStackTrace();
                msj = "Codigo de error sql: " + sqlEx.getErrorCode();
            }

		} catch (Exception e) {
			msj = "Error desconocido: " + e.getMessage();
		}
		
		return msj;
	}

	@Override
	public Tarea Consulta(int id) {
		Tarea tarea; 
		List<Tarea> tareas = new ArrayList<Tarea>();
		try{
			Connection cn = cx.abreConexion();
			CallableStatement st = cn.prepareCall(CONSULTA);
			st.setInt(1, id);
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()) {
				tareas.add(Traducir(rs));
			}
			if(tareas.size() == 1) {
				tarea = tareas.get(0);
			} else {
				tarea = null;
			}
			//msj = "Tarea guardada con exito";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
        	sqlEx.printStackTrace();
            System.out.println("Codigo de error sql: " + sqlEx.getErrorCode());
            tarea = null;
		} catch (Exception e) {
			System.out.println("Error desconocido: " + e.getMessage());
			tarea = null;
		}
		
		return tarea;
	}
	
	public List<Tarea> ConsultaIdProyecto(int id) {
		List<Tarea> tareas = new ArrayList<Tarea>();
		try{
			Connection cn = cx.abreConexion();
			CallableStatement st = cn.prepareCall(CONSULTA_ID_PROYECTO);
			st.setInt(1, id);
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()) {
				tareas.add(Traducir(rs));
			}
			if(tareas.isEmpty()) {
				tareas = null;
			}
			//msj = "Tarea guardada con exito";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			System.out.println("Codigo de error sql: " + sqlEx.getErrorCode());
			tareas = null;
		} catch (Exception e) {
			System.out.println("Error desconocido: " + e.getMessage());
			tareas = null;
		}
		
		return tareas;
	}

	@Override
	public List<Tarea> TraeTodos() {
		List<Tarea> tareas = new ArrayList<Tarea>();
		try{
			Connection cn = cx.abreConexion();
			CallableStatement st = cn.prepareCall(TODOS);
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()) {
				tareas.add(Traducir(rs));
			}
			if(tareas.isEmpty()) {
				tareas = null;
			}
			//msj = "Tarea guardada con exito";
			cx.cierraConexion();
			
		} catch (SQLException sqlEx) {
        	sqlEx.printStackTrace();
            System.out.println("Codigo de error sql: " + sqlEx.getErrorCode());
            tareas = null;
		} catch (Exception e) {
			System.out.println("Error desconocido: " + e.getMessage());
			tareas = null;
		}
		
		return tareas;
	}
	
	@Override
	public Tarea Traducir(ResultSet rs) throws SQLException {
		return new Tarea(rs.getInt("id"), rs.getInt("idProyecto"), rs.getString("nombre"), rs.getString("descripcion"), rs.getInt("duracion"), rs.getInt("predecesor"), rs.getInt("avance"), rs.getTimestamp("fechaInicio").toLocalDateTime());
	}

}
