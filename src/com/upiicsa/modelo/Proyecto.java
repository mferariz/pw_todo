package com.upiicsa.modelo;

import java.time.LocalDateTime;

public class Proyecto {

	private int id;
	private String nombre;
	private String descripcion;
	private LocalDateTime fechaEntrega;
	
	public Proyecto() {
		
	}
	
	public Proyecto(int id, String nombre, String descripcion, LocalDateTime fechaEntrega) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaEntrega = fechaEntrega;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDateTime getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(LocalDateTime fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	@Override
	public String toString() {
		return "Proyecto [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", fechaEntrega="
				+ fechaEntrega + "]";
	}
	
}
