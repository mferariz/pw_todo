package com.upiicsa.modelo;

import java.time.LocalDateTime;

/**
 * @author m_fer
 *
 */
public class Tarea {
	
	private int id;
	private int idProyecto;
	private String nombre;
	private String descripcion;
	private int duracion;
	private int predecesor;
	private int avance;
	private LocalDateTime fechaInicio;
	
	public Tarea(int id, int idProyecto, String nombre, String descripcion, int duracion, int predecesor, int avance, LocalDateTime fechaInicio) {
		super();
		this.id = id;
		this.idProyecto = idProyecto;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.duracion = duracion;
		this.predecesor = predecesor;
		this.avance = avance;
		this.fechaInicio = fechaInicio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getPredecesor() {
		return predecesor;
	}

	public void setPredecesor(int predecesor) {
		this.predecesor = predecesor;
	}

	public int getAvance() {
		return avance;
	}

	public void setAvance(int avance) {
		this.avance = avance;
	}

	public LocalDateTime getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(LocalDateTime fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Tarea [id=" + id + ", idProyecto=" + idProyecto + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", duracion=" + duracion + ", predecesor=" + predecesor + ", avance=" + avance + ", fechaInicio="
				+ fechaInicio + "]";
	}
	
}
