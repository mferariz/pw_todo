package com.upiicsa.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
	
	public static final int MYSQL_DUPLICATE_PK = 1062;
	
	Connection cn;
    public Connection abreConexion() throws Exception{
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ToDo?&serverTimezone=UTC","root","n0m3l0");
            return cn;
        }catch(Exception e){
            System.out.println(e.getMessage());
            throw e;
        }
    }
    
    public void cierraConexion() throws Exception{
        try{
            cn.close();
        }catch(Exception e){
            throw e;
        }
    }


}
